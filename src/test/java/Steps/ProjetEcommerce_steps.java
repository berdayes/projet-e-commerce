package Steps;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.junit.Assert.fail;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class ProjetEcommerce_steps {
	
	
	WebDriver driver;
	String vName = "titi";
	String vLastname = "titi";
	String vEmail = "titi@gmail.com";
	String vTelephone= "8971543154";
	String vPassword = "1234567";
	String produit = "iPhone";
	
	//*************L'utilisateur peut s'inscrire dans l'application en fournissant tous les champs obligatoire. **********//
	
	@Given("Je me suis connecte sur ECommerce")
	public void je_me_suis_connecte_sur_e_commerce() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://tutorialsninja.com/demo/");
	}

	@When("Je clique sur My Account")
	public void je_clique_sur_my_account() throws InterruptedException {
		String MyAccount="//span[normalize-space()='My Account']";
		driver.findElement(By.xpath(MyAccount)).click();
		Thread.sleep(2000);
		
	}

	@And("je clique sur Register")
	public void je_clique_sur_register() throws InterruptedException {	
		String Register="//ul[@class='dropdown-menu dropdown-menu-right']//a[normalize-space()='Register']";		
		driver.findElement(By.xpath(Register)).click();
		Thread.sleep(2000);
	    
	}

	@Then("je remplis le formulaire avec champs obligatoire.")
	public void je_remplis_le_formulaire_avec_champs_obligatoire() throws InterruptedException {
		//element web
		String Firtname="//input[@id='input-firstname']";
		String Lastname="//input[@id='input-lastname']";
		String Email="//input[@id='input-email']";
		String Telephone="//input[@id='input-telephone']";
		String Password="//input[@id='input-password']";
		String Confirme_Password="//input[@id='input-confirm']";
		String btn_Continue = "//input[@value='Continue']";
		
		//Remplir le formulaire
		driver.findElement(By.xpath(Firtname)).sendKeys(vName);
		driver.findElement(By.xpath(Lastname)).sendKeys(vLastname);
		driver.findElement(By.xpath(Email)).sendKeys(vEmail);
		driver.findElement(By.xpath(Telephone)).sendKeys(vTelephone);
		driver.findElement(By.xpath(Password)).sendKeys(vPassword);
		driver.findElement(By.xpath(Confirme_Password)).sendKeys(vPassword);
		Thread.sleep(2000);
		//acepter la Private Polity 
		driver.findElement(By.xpath("//input[@name='agree']")).click();
		//Cliquer sur le bouton Continue
		driver.findElement(By.xpath(btn_Continue)).click();
						
		
	}
	
	@And("je valide le message de Account enregistre")
	public void je_valide_le_message_de_account_enregistre() {
		//Valider que le msg Congratulation se montre sur la page
		driver.findElement(By.xpath("//*[@id=\"content\"]/p[1]")).isDisplayed();
		
		driver.quit();
		
	}
	
	//********L'utilisateur peut se connecter avec des informations d'identification valides. *******//
	
	@When("je clique sur Login")
	public void je_clique_sur_login() {
		String Login = "//ul[@class='dropdown-menu dropdown-menu-right']//a[normalize-space()='Login']";
		driver.findElement(By.xpath(Login)).click();
		
	}

	@Then("je remplis le user et mot de passe plus bouton login")
	public void je_remplis_le_user_et_mot_de_passe_plus_bouton_login() throws InterruptedException {
		String input_email = "//input[@id='input-email']";
		String input_pass = "//input[@id='input-password']";
		String btn_login = "//input[@value='Login']";
				
		driver.findElement(By.xpath(input_email)).sendKeys(vEmail);
		Thread.sleep(2000);
		driver.findElement(By.xpath(input_pass)).sendKeys(vPassword);
		driver.findElement(By.xpath(btn_login)).click();
			  
	}
	@Then("je valide que l utilisateur est connecte")
	public void je_valide_que_l_utilisateur_est_connecte() {
		String logout = "//a[@class='list-group-item'][normalize-space()='Logout']";
		//valider que la page montre le element logout
		driver.findElement(By.xpath(logout )).isDisplayed();
		
		driver.quit();
			  
	}
	
	// **************Connexion au compte password invalide*************//
	
	@Then("je remplis le user et mot de passe pas correctement plus bouton login")
	public void je_remplis_le_user_et_mot_de_passe_pas_correctement_plus_bouton_login() throws InterruptedException {
		String input_email = "//input[@id='input-email']";
		String input_pass = "//input[@id='input-password']";
		String btn_login = "//input[@value='Login']";
		
		driver.findElement(By.xpath(input_email)).sendKeys(vEmail);
		Thread.sleep(2000);
		driver.findElement(By.xpath(input_pass)).sendKeys(vPassword+2);
		driver.findElement(By.xpath(btn_login)).click();
		Thread.sleep(2000);
	}

	@Then("je valide une message d error")
	public void je_valide_une_message_d_error() {
		String message = "//div[contains(@class,'alert alert-danger alert-dismissible')]";
		
		driver.findElement(By.xpath(message)).isDisplayed();
		driver.findElement(By.xpath(message)).getText();		
		message.contains("Warning: No match for E-Mail Address and/or Password.");
						
		driver.quit();

	}
	
	//*********Recherche d un produit**************//

	@When("je fais la recherche de produit")
	public void je_fais_la_recherche_de_produit() throws InterruptedException {
		
		driver.findElement(By.xpath("//input[@name='search']")).sendKeys(produit);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[contains(@class,'btn-default')]")).click();

	}
	@Then("Je valide le resultat")
	public void je_valide_le_resultat() {
		String part1 = "//a[normalize-space()='";
		String part2 = "']";
		
		//Valider que le nom du produit se montre sur la page 
		if (!(driver.findElement(By.xpath(part1 + produit + part2)).isDisplayed())) {
			fail();
		}
		driver.quit();
		
	}


	// ****pas produit disponible*****//


	@When("je recherche des produits")
	public void je_recherche_des_produits() throws InterruptedException {
		
		driver.findElement(By.xpath("//input[@name='search']")).sendKeys("TCL");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[contains(@class,'btn-default')]")).click();
		Thread.sleep(2000);
	}
	@Then("Je valide le resultat pas disponible")
	public void je_valide_le_resultat_pas_disponible() {
		
		String text = driver.findElement(By.xpath("//*[@id=\"content\"]/p[2]")).getText();
		
		if(!(text.contains("There is no product that matches the search criteria."))) {
			fail();
		}
		driver.quit();

	}

					

}
