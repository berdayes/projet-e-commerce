package testRuner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"featchers"}, 
		glue = {"Steps"},
		monochrome = true,
		dryRun = false,
		//name="search"
		//tags="@tag5",
		//generer rapport
		plugin= {"pretty","html:rapport1"} 
			
		)
public class TestRunner {

}
