#Author: Equipe 3 (Andro,Rodio,Samir)

Feature: Liste de scenario de validation cast de test pour le projet final de Automatisation

  @tag1
  Scenario: L utilisateur peut s inscrire dans l application en fournissant tous les champs obligatoire.
    Given Je me suis connecte sur ECommerce
    When Je clique sur My Account
    And je clique sur Register
    Then je remplis le formulaire avec champs obligatoire.
    And je valide le message de Account enregistre

  @tag2
  Scenario: l utilisateur peut se connecter avec des informations d identification valides.
    Given Je me suis connecte sur ECommerce
    When Je clique sur My Account
    And je clique sur Login
    Then je remplis le user et mot de passe plus bouton login
    And je valide que l utilisateur est connecte

  @tag3
  Scenario: l utilisateur se connecter avec des informations d identification invalides.
    Given Je me suis connecte sur ECommerce
    When Je clique sur My Account
    And je clique sur Login
    Then je remplis le user et mot de passe pas correctement plus bouton login
    And je valide une message d error

  @tag4
  Scenario: L utilisateur est en mesure de rechercher des produits.
    Given Je me suis connecte sur ECommerce
    When je fais la recherche de produit
    Then Je valide le resultat

  @tag5
  Scenario: L utilisateur est informe lorsque le produit recherche n est pas disponible.
    Given Je me suis connecte sur ECommerce
    When je recherche des produits
    Then Je valide le resultat pas disponible
